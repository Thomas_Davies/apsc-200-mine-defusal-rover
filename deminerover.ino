/* TO WORK:
    -3 different types of mines (detect with IR & metal Sense)
        1. Black & metal
        2. Silver Metal (just not black)
        3. Non metal black
    -Large object with height higher than rover placed at end of line
        - Ultrasonic Sensor on arm finds distance!
    - This model only follows straight path. 
*/  
/*                                 FUNCTIONS
HEADER                                  PURPOSE                                              COMPLETE
void servoState(char input[])            sets servo motors to called state                      y
void driveState(char input[])            sets drive motors to called state                      y
int IRSense ()                           checks ir values, returns 1 if black                   y
int mineDetect()                        calls metal and color detect functions                  y
                                         returns 1 if mine and change type array
void mineFound()                        light leds and makes noise                              n
void Mdetect()                          checks voltage from metal detect circuit                n
int distanceCheck()                     returns distance(cm) from wall                          n
*/
#include <Servo.h>

//ULTRASONIC SENSOR PINS
#define trigPin 13
#define echoPin 12

//MOTORS
int E1 =6; //M1 Speed Control
int E2 =5; //M2 Speed Control
int M1 =8; //M1 Direction Control
int M2 = 7; //M2 Direction Control

Servo leftServo;
Servo rightServo;

char minetype[5]; //global to avoid C array restrictions :(

void setup()
{
  int i;
  for(i=5;i<=8;i++)
  pinMode(i, OUTPUT);
  
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  
  leftServo.attach(10);
  rightServo.attach(11);
  
  Serial.begin(9600);
}
void loop()
{
  char type[5];
  int mineCount[3]; //0 == black metal, 1 == metal, 2 == black
  long initialDistance;
  long currDistance;
  //set mine count array to zero
  for (int i = 0;i < 3;i++)
    mineCount[i] = 0;
    
  initialDistance = distanceCheck();
  
  while ((int)(initialDistance - distanceCheck()) < 100)
  {  
    /* DRIVE FORWARD, IF MINE DETECTED[ SAVE TYPE COUNT TO ARRAY, demine the bitch] reloop */
    driveState("FORWARD");
    servoState("DOWN");
    if (mineDetect()) {
      mineFound();
      driveState("STOP");
      servoState("DEMINE");
      delay(500);
      if (type == "BM")
        mineCount[0] ++;  
      else if (type == "BNM")
        mineCount[1] ++;
      else if (type == "MM")
        mineCount[2] ++;
    }  
    driveState("FORWARD");
    servoState("DOWN");
   }
    
}

void servoState(char input[])
{
  if (input == "DOWN"){
         leftServo.write(5);
         rightServo.write(175);
  }
  else if (input == "DEMINE"){
    leftServo.write(60);
    rightServo.write(120);
    delay(1000);
    leftServo.write(5);
    rightServo.write(175);
  }
}

void driveState(char input[]){
  if(input == "FORWARD"){    
      analogWrite (E1,255);
      digitalWrite(M1,LOW);
      analogWrite (E2,255);
      digitalWrite(M2,LOW);
  }
  else if (input == "REVERSE"){
      analogWrite (E1,255);
      digitalWrite(M1,HIGH);
      analogWrite (E2,255);
      digitalWrite(M2,HIGH);
  }
  else if (input == "STOP"){
      analogWrite (E1,0);
      analogWrite (E2,0);
  }
}
//returns 1 if black 0 if not
int IRSense (){
  int left = analogRead(A0);
  int center = analogRead(A1);
  int right = analogRead(A2);
  
  Serial.print("LEFT: ");
  Serial.println(left); //left is not very sensitive. may skew results. Will remove.
  Serial.print("MID: ");
  Serial.println(center);
  Serial.print("RIGHT ");
  Serial.println(right);
  
  if ((center <100) || (right < 300))
    return 1;
  else
    return 0;
}

/*when called pass by reference ex. mineDetect(&found,&type);
THIS FUNCTION simulates usage of metal detect and ground penetrating radar.
using ir sensor and inductor coil it detects if mine found and what type it is*/

int mineDetect(){
  /*return 1 if any mound found
    type = "BM" = black metal mine
           "BNM" = black non metal mine
           "MM" = metal non black mine
  */
  if ((IRSense() == 1) && (Mdetect() == 1))
  {
     strcpy(minetype,"BM");
     return 1;
  }
  else if (IRSense() == 1)
  {
    strcpy(minetype,"BNM");
    return 1;
  }
  else if (Mdetect() == 1)
  {
    strcpy(minetype,"MM");
    return 1;
  }
  else
    return 0;
}

//mine found personnel response. PLAY BUZZER AND FLASH LEDS
void mineFound(){
  //WOO WOO WOO WOO WOO WOO WOO WOO
}

//scan inductor coil niggah
int Mdetect (){
  return 1;
}

long distanceCheck () {
  long duration,distance;
  digitalWrite(trigPin,LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  duration - pulseIn(echoPin,HIGH);
  distance = (duration/2)/29.1;
  return distance;
}
  


